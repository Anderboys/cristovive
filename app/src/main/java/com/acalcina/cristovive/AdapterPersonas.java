package com.acalcina.cristovive;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.recyclerview.widget.RecyclerView;
import com.acalcina.cristovive.Modelo.Persona;

import java.util.ArrayList;




public class AdapterPersonas extends RecyclerView.Adapter<AdapterPersonas.ViewHolderPeronas>{
        //datos
        ArrayList<Persona> listapersonas  = new ArrayList<Persona>();
    //3.
        Context context;


                                              //4. agregar , Context context
    public AdapterPersonas(ArrayList<Persona> listausuarios , Context context){

        this.listapersonas = listausuarios;
        //5.
        this.context=context;
    }

    @Override
    public ViewHolderPeronas onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.itempersona,parent,false);

        return new ViewHolderPeronas(view,context,listapersonas);


    }

    @Override
    public void onBindViewHolder(ViewHolderPeronas holder, int position) {

        holder.textnombre.setText(listapersonas.get(position).getNom());
        holder.textApellido.setText(listapersonas.get(position).getApe());
        holder.texttelefono.setText(listapersonas.get(position).getTelefono());
    }

    @Override
    public int getItemCount() {
        return listapersonas.size();
    }

        public class ViewHolderPeronas extends RecyclerView.ViewHolder implements View.OnClickListener {


        TextView textnombre,textApellido,texttelefono;
        ArrayList<Persona> personas = new ArrayList<Persona>();
        Context ctx;


        public ViewHolderPeronas(View itemView,Context ctx,ArrayList<Persona> personas) {
            super(itemView);

            this.personas = personas;
            this.ctx=ctx;

            //3. AGREGAR  itemView.setOnClickListener(this);
            itemView.setOnClickListener(this);

            textnombre = (TextView)itemView.findViewById(R.id.textNombre);
            textApellido = (TextView)itemView.findViewById(R.id.textApellido);
            texttelefono = (TextView)itemView.findViewById(R.id.textTelefono);

        }
            @Override
            public void onClick(View v) {

                int position = getAdapterPosition();
                Persona persona = this.personas.get(position);
                Intent intent = new Intent(this.ctx,DetallesPersona.class);

                intent.putExtra("ID",persona.getId());
                intent.putExtra("NOMBRE",persona.getNom());
                intent.putExtra("APELLIDO",persona.getApe());
                intent.putExtra("DISTRITO",persona.getDireccion());
                intent.putExtra("TELEFONO",persona.getTelefono());
                intent.putExtra("ESTADO",persona.getEstado());
                intent.putExtra("DNI",persona.getDni());
                intent.putExtra("FECHANACI",persona.getFechanaci());

                this.ctx.startActivity(intent);
            }
        }
}
