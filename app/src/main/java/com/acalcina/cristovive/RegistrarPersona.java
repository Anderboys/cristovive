package com.acalcina.cristovive;

import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;
import androidx.appcompat.app.AppCompatActivity;


public class RegistrarPersona extends AppCompatActivity {

    Button btngrabar,btnabrirfecha2;
    EditText txtNom,txtApe,txtdirec,txtfono,txtdni;
    TextView fecharegistro;
    DatePickerDialog datePickerDialog;
    CheckBox chk1,chk2;
    String estado="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registrar_persona);

        txtNom=(EditText)findViewById(R.id.txtNom);
        txtApe=(EditText)findViewById(R.id.txtApellido);
        txtdirec=(EditText)findViewById(R.id.txtdireccion);
        txtfono=(EditText)findViewById(R.id.txtfono);
        //  txtestado=(EditText)findViewById(R.id.txtestado);
        txtdni=(EditText)findViewById(R.id.txtdni);
        btnabrirfecha2=(Button)findViewById(R.id.btnabrirfecha2);
        fecharegistro = (TextView)findViewById(R.id.fecharegistro);
        btngrabar=(Button)findViewById(R.id.btngrabar);

        chk1=(CheckBox)findViewById(R.id.chk1);
        chk2=(CheckBox)findViewById(R.id.chk2);

        chk1.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    estado="Soltero(a)";
                    chk2.setEnabled(false);
                }else {
                    chk2.setEnabled(true);
                }
            }
        });

        chk2.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if(isChecked){
                    estado="Casado(a)";
                    chk1.setEnabled(false);
                }else {
                    chk1.setEnabled(true);
                }
            }
        });


        btnabrirfecha2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                datePickerDialog = new DatePickerDialog(RegistrarPersona.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year,monthOfYear,dayOfMonth);
                        fecharegistro.setText(simpleDateFormat.format(newDate.getTime()));
                    }
                },mYear,mMonth,mDay);
                datePickerDialog.show();
            }
        });


        btngrabar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(txtNom.getText().toString().isEmpty()) {
                    txtNom.setError("Nombre Obligatorio");
                    txtNom.requestFocus();
                }else if(txtApe.getText().toString().isEmpty()){
                    txtApe.setError("Apellidos Obligatorio");
                    txtApe.requestFocus();
                }else if(txtdirec.getText().toString().isEmpty()){
                    txtdirec.setError("Direccion Obligatorio");
                    txtdirec.requestFocus();
                }else if(txtfono.getText().toString().isEmpty()){
                    txtfono.setError("Telefono Obligatorio");
                    txtfono.requestFocus();

                }else if(chk1.isChecked()==false && chk2.isChecked()==false ){
                    Toast.makeText(RegistrarPersona.this, "Selecionar Estado", Toast.LENGTH_SHORT).show();

                }else if(txtdni.getText().toString().isEmpty()){
                    txtdni.setError("DNI Obligatorio");
                    txtdni.requestFocus();
                }else if(fecharegistro.getText().toString().isEmpty()){
                    fecharegistro.setError("fecha Obligatorio");
                    Toast.makeText(RegistrarPersona.this, "Seleccionar Fecha", Toast.LENGTH_SHORT).show();
                }else {

                    final String nom = txtNom.getText().toString();
                    final String ape = txtApe.getText().toString();
                    final String direc = txtdirec.getText().toString() ;
                    final String fono = txtfono.getText().toString();
                    // estado
                    final String dnni = txtdni.getText().toString() ;
                    final String fecha = fecharegistro.getText().toString();

                    Thread tr = new Thread() {
                        @Override
                        public void run() {
                            registrarPer(nom, ape, direc,fono,estado,dnni,fecha);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    Toast.makeText(RegistrarPersona.this, "Persona Registrada Satisfactoriamente ", Toast.LENGTH_SHORT).show();
                                }
                            });
                        }
                    };

                    tr.start();

                    Intent intent = new Intent(getApplicationContext(),Principal.class);
                    startActivity(intent);
                }

            }
        });
    }

    ////--------------- REGISTRAR PERSONA -------------------///

    public String registrarPer(String nombre2,String apellido2,String direccion2,String telefono2,String estado2,String dni2,String fechanaci2){

        nombre2 = nombre2.replace(" ","%20");
        apellido2 = apellido2.replace(" ","%20");
        direccion2 = direccion2.replace(" ","%20");
        telefono2 = telefono2.replace(" ","%20");
        estado2 = estado2.replace(" ","%20");
        dni2 = dni2.replace(" ","%20");
        fechanaci2 = fechanaci2.replace(" ","%20");

        URL url = null;
        String linea="";
        int respuesta=0;
        StringBuilder resul=null;
        try{

            url = new URL("http://192.168.0.3:8080/WebServiceIglesia/RegistrarPersona.php?nombre="+nombre2+"&apellido="+apellido2+"&direccion="+direccion2+"&telefono="+telefono2+"&estado="+estado2+"&dni="+dni2+"&fechanaci="+fechanaci2+" ");
            //url = new URL("https://anderboys.000webhostapp.com/WebServiceIglesia/RegistrarPersona.php?nombre="+nombre2+"&apellido="+apellido2+"&direccion="+direccion2+"&telefono="+telefono2+"&estado="+estado2+"&dni="+dni2+"&fechanaci="+fechanaci2+" ");


            HttpURLConnection conection = (HttpURLConnection)url.openConnection();
            respuesta = conection.getResponseCode();

            resul = new StringBuilder();


            if(respuesta== HttpURLConnection.HTTP_OK){
                InputStream in= new BufferedInputStream(conection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                while ((linea = reader.readLine())!=null ){


                    resul.append(linea);

                }
            }
        }catch (Exception ex){
            Toast.makeText(getApplicationContext(),ex+"", Toast.LENGTH_SHORT).show();
        }


        return resul.toString();
    }
    // ....................................................................
}
