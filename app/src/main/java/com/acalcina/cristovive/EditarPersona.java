package com.acalcina.cristovive;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

public class EditarPersona extends AppCompatActivity {

    TextView tvidd;
    EditText edtnombre,edtapellido,edtdireccion,edttelefono,edtestado,edtdni;
    TextView fecharegistro2;
    Button btnguardar,btnabrirfecha;
    DatePickerDialog datePickerDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.editar_persona);

        tvidd = (TextView)findViewById(R.id.tvidd);
        edtnombre = (EditText)findViewById(R.id.edtnombre);
        edtapellido = (EditText)findViewById(R.id.edtapellido);
        edtdireccion = (EditText)findViewById(R.id.edtdireccion);

        edttelefono = (EditText)findViewById(R.id.edttelefono);
        edtestado = (EditText)findViewById(R.id.edtestado);
        edtdni = (EditText)findViewById(R.id.edtdni);

        fecharegistro2 = (TextView)findViewById(R.id.fecharegistro2);

        btnguardar = (Button)findViewById(R.id.btnguardar);
        btnabrirfecha= (Button)findViewById(R.id.btnabrirfecha);


        Bundle recibe = this.getIntent().getExtras();

        int idd = recibe.getInt("ID2");
        String nombre = recibe.getString("NOMBRE2");
        String apellido = recibe.getString("APELLIDO2");
        String distrito = recibe.getString("DISTRITO2");
        String telefono = recibe.getString("TELEFONO2");
        String estado = recibe.getString("ESTADO2");
        String dni = recibe.getString("DNI2");
        String fecha = recibe.getString("DATE2");

        tvidd.setText(idd+"");
        edtnombre.setText(nombre);
        edtapellido.setText(apellido);
        edtdireccion.setText(distrito);
        edttelefono.setText(telefono);
        edtestado.setText(estado);
        edtdni.setText(dni);
        fecharegistro2.setText(fecha);

        btnabrirfecha.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR);
                int mMonth = c.get(Calendar.MONTH);
                int mDay = c.get(Calendar.DAY_OF_MONTH);
                final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy", Locale.US);
                datePickerDialog = new DatePickerDialog(EditarPersona.this, new DatePickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                        Calendar newDate = Calendar.getInstance();
                        newDate.set(year,monthOfYear,dayOfMonth);
                        fecharegistro2.setText(simpleDateFormat.format(newDate.getTime()));
                    }
                },mYear,mMonth,mDay);
                datePickerDialog.show();
            }
        });

        btnguardar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String nombre2 = edtnombre.getText().toString();
                final  String apellido2 = edtapellido.getText().toString();
                final  String direccion2 = edtdireccion.getText().toString();
                final   String telefono2 = edttelefono.getText().toString();
                final   String estado2 = edtestado.getText().toString();
                final  String dni2 = edtdni.getText().toString();
                final  String fecha2 = fecharegistro2.getText().toString();

                final  Integer idusu = Integer.parseInt(tvidd.getText().toString());

                Thread tr = new Thread(){

                    public void run(){
                        enviarPersonasGet(nombre2,apellido2,direccion2,telefono2,estado2,dni2,fecha2,idusu);
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(EditarPersona.this, "Persona Editado", Toast.LENGTH_SHORT).show();
                            }
                        });
                    }
                };
                tr.start();
                Intent intent = new Intent(getApplicationContext(),Principal.class);
                startActivity(intent);

            }
        });
    }

    public String enviarPersonasGet(String nombre, String apellido,String direccion,String telefono,String estado,String dni,String fecha,int cod){

        nombre = nombre.replace(" ","%20");
        apellido = apellido.replace(" ","%20");
        direccion = direccion.replace(" ","%20");
        telefono = telefono.replace(" ","%20");
        estado = estado.replace(" ","%20");
        dni = dni.replace(" ","%20");
        fecha = fecha.replace(" ","%20");

        URL url = null;
        String linea="";
        int respuesta=0;
        StringBuilder resul=null;
        try{


            url = new URL("http://192.168.0.3:8080/WebServiceIglesia/EditarPersona.php?nombre="+nombre+"&apellido="+apellido+"&direccion="+direccion+"&telefono="+telefono+"&estado="+estado+"&dni="+dni+"&fechanaci="+fecha+"&id="+cod+" ");
            //url = new URL("https://anderboys.000webhostapp.com/WebServiceIglesia/EditarPersona.php?nombre="+nombre+"&apellido="+apellido+"&direccion="+direccion+"&telefono="+telefono+"&estado="+estado+"&dni="+dni+"&fechanaci="+fecha+"&id="+cod+" ");


            HttpURLConnection conection = (HttpURLConnection)url.openConnection();
            respuesta = conection.getResponseCode();
            resul = new StringBuilder();

            if(respuesta== HttpURLConnection.HTTP_OK){
                InputStream in= new BufferedInputStream(conection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                while ((linea = reader.readLine())!=null ){
                    resul.append(linea);
                }
            }
        }catch (Exception ex){
            Toast.makeText(getApplicationContext(),ex+"", Toast.LENGTH_SHORT).show();
        }

        return resul.toString();
    }



    // 1.  ------------ this is For ACTION BAR -------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_editar, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.noGuardar) {
            this.finish();
            return true;
        }


        return super.onOptionsItemSelected(item);
    }

    //----------------------------------------------------------------
}
