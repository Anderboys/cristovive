package com.acalcina.cristovive;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONArray;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

//¿Como darle formato (ordenar) al código en el IDE Itellij IDEA?  = Control + Alt + L
public class Login extends AppCompatActivity {

    EditText txtusu, txtpas;
    Button btnlogin;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        txtusu = (EditText) findViewById(R.id.txtusu);
        txtpas = (EditText) findViewById(R.id.txtpas);
        btnlogin = (Button) findViewById(R.id.btningresar);

        btnlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                {
                    final String usu = txtusu.getText().toString();
                    final String pas = txtpas.getText().toString();

                    Thread tr = new Thread() {
                        @Override
                        public void run() {    // creamos metodo run clip derecho Generate - Override Methods - Run()Void
                            final String resultado = loginusu(usu, pas);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    int r = obtDatosJSON(resultado);
                                    if (r > 0) {

                                        Intent intent = new Intent(getApplicationContext(), Principal.class);
                                        startActivity(intent);

                                    } else {
                                        Toast.makeText(Login.this, "Usuario o pas Incorrectos", Toast.LENGTH_SHORT).show();
                                    }
                                }
                            });
                        }
                    };
                    tr.start();
                }
            }
        });
    }

    public String loginusu(String usu, String pass) {

        URL url = null;
        String linea = "";
        int respuesta = 0;
        StringBuilder resul = null;

        try {
            url = new URL("http://192.168.0.3:8080/WebServiceIglesia/LoginAdmin.php?usu=" + usu + "&pas=" + pass + "");
            //url = new URL("https://anderboys.000webhostapp.com/WebServiceIglesia/LoginAdmin.php?usu=" + usu + "&pas=" + pass + "");
            HttpURLConnection conection = (HttpURLConnection) url.openConnection();
            // aqui esta que cae : respuesta = conection.getResponseCode();
            // si hay error de: Cleartext HTTP traffic not permitted
            // Agregar en el AndroidManifest.xml  : <application android:usesCleartextTraffic="true" >
            respuesta = conection.getResponseCode();
            resul = new StringBuilder();

            if (respuesta == HttpURLConnection.HTTP_OK) {
                InputStream in = new BufferedInputStream(conection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                while ((linea = reader.readLine()) != null) {
                    resul.append(linea);
                }
            }
        } catch (Exception ex) {
            Toast.makeText(getApplicationContext(), ex + "", Toast.LENGTH_SHORT).show();
        }

        return resul.toString();
    }

    public int obtDatosJSON(String respuesta) {
        int res = 0;

        try {
            JSONArray json = new JSONArray(respuesta);
            if (json.length() > 0) {
                res = 1;
            }

        } catch (Exception e) {
        }

        return res;
    }
}