package com.acalcina.cristovive.Modelo;

/**
 * Created by Windows on 22/08/2017.
 */

public class Persona {

    private int id;
    private String nom,ape,direccion,telefono,estado,dni,fechanaci;

    public Persona(int id, String nom, String ape, String direccion, String telefono, String estado, String dni, String fechanaci) {
        this.id = id;
        this.nom = nom;
        this.ape = ape;
        this.direccion = direccion;
        this.telefono = telefono;
        this.estado = estado;
        this.dni = dni;
        this.fechanaci = fechanaci;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getApe() {
        return ape;
    }

    public void setApe(String ape) {
        this.ape = ape;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getFechanaci() {
        return fechanaci;
    }

    public void setFechanaci(String fechanaci) {
        this.fechanaci = fechanaci;
    }

    @Override
    public String toString() {
        return
                "nom='" + nom + '\n' +
                "ape='" + ape + '\n' +
                "telefono='" + telefono
                ;


    }
}
