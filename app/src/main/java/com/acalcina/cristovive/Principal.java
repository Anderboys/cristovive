package com.acalcina.cristovive;

import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import org.json.JSONArray;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.acalcina.cristovive.Modelo.Persona;
import com.google.android.material.floatingactionbutton.FloatingActionButton;


public class Principal extends AppCompatActivity {

    FloatingActionButton fab2;
    EditText edtnombre2;
    Button btnbuscar2;
    RecyclerView recyclerview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.principal);

        edtnombre2 = (EditText) findViewById(R.id.edtnombre2);
        btnbuscar2 = (Button) findViewById(R.id.btnbuscar2);
        fab2 = (FloatingActionButton) findViewById(R.id.fab);
        recyclerview = (RecyclerView) findViewById(R.id.recyclerpersona);

        //cuarto final Listar recyclerview principal.xml.
        listarpersonasMain();

        fab2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(getApplicationContext(), RegistrarPersona.class);
                startActivity(intent);


            }
        });

        btnbuscar2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final String nombre = edtnombre2.getText().toString();

                if(edtnombre2.getText().toString().isEmpty()) {
                    edtnombre2.setError("Ingresar Nombre a Buscar");
                    edtnombre2.requestFocus();

                }else{

                    Thread tr2 = new Thread(){
                        @Override
                        public void run(){
                            final String resultado=consultarPersona(nombre);
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    cargaListado(obtDatosJSON(resultado));
                                }
                            });
                        }
                    };
                    tr2.start();

                    edtnombre2.setText("");
                    edtnombre2.requestFocus();

                }
            }
        });

    }

    // primero ConsultarPersona.php --------------- CONSULTAR Personas EN RECYCLERVIEW -------------------///
    public String consultarPersona(String nombre){

        URL url = null;
        String linea="";
        int respuesta=0;
        StringBuilder resul=null;
        try{

            url = new URL("http://192.168.0.3:8080/WebServiceIglesia/ConsultarPersona.php?nom="+nombre+"");
                                // http://192.168.0.3:8080/WebServiceIglesia/ConsultarPersona.php?nom=Astupinauro
            //url = new URL("https://anderboys.000webhostapp.com/WebServiceIglesia/ConsultarPersona.php?nom="+nombre+"");

            HttpURLConnection conection = (HttpURLConnection)url.openConnection();
            respuesta = conection.getResponseCode();
            resul = new StringBuilder();

            if(respuesta== HttpURLConnection.HTTP_OK){
                InputStream in= new BufferedInputStream(conection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                while ((linea = reader.readLine())!=null ){
                    resul.append(linea);
                }
            }
        }catch (Exception ex){
            Toast.makeText(getApplicationContext(),ex+"", Toast.LENGTH_SHORT).show();
        }

        return resul.toString();
    }
    //...................................................................



    //primero ListarPersonas.php --------------- LISTAR PERSONAS EN RECYCLERVIEW -------------------//
    public String listarPersona(){

        URL url = null;
        String linea="";
        int respuesta=0;
        StringBuilder resul=null;
        try{

            url = new URL("http://192.168.0.3:8080/WebServiceIglesia/ListarPersonas.php");
         //   url = new URL("https://anderboys.000webhostapp.com/WebServiceIglesia/ListarPersonas.php");

            HttpURLConnection conection = (HttpURLConnection)url.openConnection();
            respuesta = conection.getResponseCode();
            resul = new StringBuilder();

            if(respuesta== HttpURLConnection.HTTP_OK){
                InputStream in= new BufferedInputStream(conection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                while ((linea = reader.readLine())!=null ){
                    resul.append(linea);
                }
            }
        }catch (Exception ex){
            Toast.makeText(getApplicationContext(),ex+"", Toast.LENGTH_SHORT).show();
        }

        return resul.toString();
    }

    //segundo ListarPersonas.php  y para ConsultarPersona.php
    public ArrayList<Persona> obtDatosJSON(String response){
        ArrayList<Persona> listado = new ArrayList<Persona>();
        try{

            JSONArray json = new JSONArray(response);

            for(int i=0;i<json.length();i++){
                Persona usu= new Persona(json.getJSONObject(i).getInt("id"),
                        json.getJSONObject(i).getString("nombre"),
                        json.getJSONObject(i).getString("apellido"),
                        json.getJSONObject(i).getString("direccion"),    // campos - tabla persona de la DB dbiglesia
                        json.getJSONObject(i).getString("telefono"),
                        json.getJSONObject(i).getString("estado"),
                        json.getJSONObject(i).getString("dni"),
                        json.getJSONObject(i).getString("fechanaci"));
                listado.add(usu);
            }
        }catch (Exception e){
            Toast.makeText(getApplicationContext(),e+"", Toast.LENGTH_SHORT).show();
        }
        return listado;
    }

    //tercero ConsultarPersona.php
    public  void cargaListado(ArrayList<Persona> datos){

        recyclerview.setLayoutManager(new LinearLayoutManager(this));
        //5. AGREGAR ,this
        AdapterPersonas adapterPersonas = new AdapterPersonas(datos,this);
        recyclerview.setAdapter(adapterPersonas);

    }

    //...................................................................



    //tercero ListarPersonas.php ------   METODO PARA CARGAR DATOS EN EL LOAD (onCreate)  ------
    public void listarpersonasMain(){

        Thread tr = new Thread(){
            @Override
            public void run(){    // creamos metodo run clip derecho Generate - Override Methods - Run()Void
                final String resultado=listarPersona();
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        cargaListado(obtDatosJSON(resultado));
                    }
                });
            }
        };
        tr.start();
    }
    //--------------------------------------------------------------------------


    // 1.  ------------ this is For ACTION BAR -------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_refresh, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {

            case R.id.refresh:

                listarpersonasMain();
                return true;

            case R.id.logout:

                Intent intent = new Intent(getApplicationContext(),Login.class);
                startActivity(intent);

                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }
    //----------------------------------------------------------------

}