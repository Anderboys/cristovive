package com.acalcina.cristovive;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class DetallesPersona extends AppCompatActivity {

    TextView tvid,tvnombre,tvapellido,tvdireccion,tvfono,tvestado,tvdni,tvdate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detalles_persona);

        tvid=(TextView)findViewById(R.id.tvid);
        tvnombre=(TextView)findViewById(R.id.tvnombre);
        tvapellido=(TextView)findViewById(R.id.tvapellido);
        tvdireccion=(TextView)findViewById(R.id.tvdireccion);
        tvfono=(TextView)findViewById(R.id.tvfono);
        tvestado=(TextView)findViewById(R.id.tvestado);
        tvdni=(TextView)findViewById(R.id.tvdni);
        tvdate=(TextView)findViewById(R.id.tvdate);

        Bundle recibe = this.getIntent().getExtras();

        int idd = recibe.getInt("ID");
        tvid.setText(idd+"");

        String nombre = recibe.getString("NOMBRE");
        tvnombre.setText(nombre);

        String apellido = recibe.getString("APELLIDO");
        tvapellido.setText(apellido);

        String direccion = recibe.getString("DISTRITO");
        tvdireccion.setText(direccion);

        String telefono = recibe.getString("TELEFONO");
        tvfono.setText(telefono);

        String estado = recibe.getString("ESTADO");
        tvestado.setText(estado);

        String dni = recibe.getString("DNI");
        tvdni.setText(dni);

        String fecha = recibe.getString("FECHANACI");
        tvdate.setText(fecha);

    }

    // 1.  ------------ this is For ACTION BAR -------------
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_detalles, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()){

            case R.id.editt:

                Intent intent = new Intent(getApplicationContext(),EditarPersona.class);

                Integer id2 = Integer.parseInt(tvid.getText().toString()) ;
                String nombre2 = tvnombre.getText().toString();
                String apellido2 = tvapellido.getText().toString();
                String direccion2 = tvdireccion.getText().toString();
                String telefono2 = tvfono.getText().toString();
                String estado2 = tvestado.getText().toString();
                String dni2 = tvdni.getText().toString();
                String date2 = tvdate.getText().toString();


                Bundle b = new Bundle();
                b.putInt("ID2",id2);
                b.putString("NOMBRE2",nombre2);
                b.putString("APELLIDO2",apellido2);
                b.putString("DISTRITO2",direccion2);
                b.putString("TELEFONO2",telefono2);
                b.putString("ESTADO2",estado2);
                b.putString("DNI2",dni2);
                b.putString("DATE2",date2);
                intent.putExtras(b);

                startActivity(intent);


                return true;

            case R.id.delete:

                String nombre = tvnombre.getText().toString();

                AlertDialog.Builder alert = new AlertDialog.Builder(DetallesPersona.this);
                alert.setTitle("Alerta!");
                alert.setMessage("Desea Eliminar a ? " +nombre);
                alert.setPositiveButton("ACEPTAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        ElimnarThreah();

                        Intent intent = new Intent(getApplicationContext(),Principal.class);
                        startActivity(intent);

                    }
                }).setNegativeButton("CANCELAR", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                alert.create();
                alert.show();


                return true;


            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //----------------------------------------------------------------

    public void ElimnarThreah(){

        final int iddd = Integer.parseInt(tvid.getText().toString()) ;

        Thread tr = new Thread(){
            @Override
            public void run(){
                eliminarPer(iddd);
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        Toast.makeText(DetallesPersona.this, "usuario elimnado Ok", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        };
        tr.start();

        Intent intent = new Intent(getApplicationContext(),Principal.class);
        startActivity(intent);

    }

    //--------- METODO ELIMINAR HttpURLConnection ----------------

    public String eliminarPer(int idd){

        URL url = null;
        String linea="";
        int respuesta=0;
        StringBuilder resul=null;
        try{

            url = new URL("http://192.168.0.3:8080/WebServiceIglesia/EliminarPersona.php?id="+idd+"");
            //url = new URL("https://anderboys.000webhostapp.com/WebServiceIglesia/EliminarPersona.php?id="+idd+"");

            HttpURLConnection conection = (HttpURLConnection)url.openConnection();
            respuesta = conection.getResponseCode();
            resul = new StringBuilder();

            if(respuesta== HttpURLConnection.HTTP_OK){
                InputStream in= new BufferedInputStream(conection.getInputStream());
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                while ((linea = reader.readLine())!=null ){
                    resul.append(linea);
                }
            }
        }catch (Exception ex){
            Toast.makeText(getApplicationContext(),ex+"", Toast.LENGTH_SHORT).show();
        }

        return resul.toString();
    }

}
